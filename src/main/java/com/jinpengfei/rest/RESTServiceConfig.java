package com.jinpengfei.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by jinpengfei on 16-11-4.
 */
@ApplicationPath("/")
public class RESTServiceConfig extends ResourceConfig {
    public RESTServiceConfig() {

        //服务类所在的包路径
        packages("com.jinpengfei.rest.resource");
        //注册JSON转换器
        register(JacksonJsonProvider.class);

    }
}